import max86150
import display


class SPO2:
    def __init__(self):
        self.sensor = None
        self.RATE = 128
        self.HISTORY_MAX = self.RATE * 4
        self.history = []
        self.update_screen = 0
        self.disp = display.open()
        self.DRAW_AFTER_SAMPLES = 5
        self.histogram_offset = 0
        self.WIDTH = 160
        self.SCALE_FACTOR = 30
        self.OFFSET_Y = 49
        self.COLOR_BACKGROUND = [0, 0, 0]
        self.avg = [0] * 10
        self.avg_pos = 0
        self.last_sample = 0.0
        self.filtered_value = 0.0

    def open(self):
        def callback(datasets):
            self.update_screen += len(datasets)

            self.update_history(datasets)

            # don't update on every callback
            if self.update_screen >= self.DRAW_AFTER_SAMPLES:
                self.draw_histogram()

        self.sensor = max86150.MAX86150(callback)
        while True:
            pass

    def update_history(self, datasets):
        for val in datasets:
            # get red value (first in tuple)
            self.avg[self.avg_pos] = val.red
            if self.avg_pos < 9:
                self.avg_pos += 1
            else:
                self.avg_pos = 0

            avg_data = sum(self.avg) / 10
            # DC offset removal
            self.filtered_value = 0.9 * (
                self.filtered_value + avg_data - self.last_sample
            )
            self.last_sample = avg_data
            self.history.append(self.filtered_value)

        # trim old elements
        self.history = self.history[-self.HISTORY_MAX :]

    def draw_histogram(self):
        self.disp.clear(self.COLOR_BACKGROUND)

        # offset in pause_histogram mode
        window_end = len(self.history) - self.histogram_offset
        s_start = max(0, window_end - (self.RATE * 2))
        s_end = max(0, window_end)
        s_draw = max(0, s_end - self.WIDTH)

        # get max value and calc scale
        value_max = max(abs(x) for x in self.history[s_start:s_end])
        scale = self.SCALE_FACTOR / (value_max if value_max > 0 else 1)

        # draw histogram
        draw_points = (
            int(x * scale + self.OFFSET_Y) for x in self.history[s_draw:s_end]
        )

        prev = next(draw_points)
        for x, value in enumerate(draw_points):
            self.disp.line(x, prev, x + 1, value)
            prev = value

        self.disp.update()
        self.update_screen = 0

    def close(self):
        if self.self is not None:
            self.sensor.close()
            self.sensor = None


if __name__ == "__main__":
    sensor = SPO2()
    try:
        sensor.open()
    except KeyboardInterrupt as e:
        sensor.close()
        raise e
